# Changelog
All notable changes to this repository will be documented in this file.

## [0.1] - 2020-06-17
### Added
- Folders adjustment and creation of necessary files to organize the repository.
- Edit _.gitignore_ file with the exceptions required by this repository.
- Add License and another relevan information.

